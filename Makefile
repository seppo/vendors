
%.rdf: %.ttl
	rapper -i turtle -o rdfxml-abbrev $^ > $@
%.gpx: tools/rdf2gpx.xslt %.rdf
	xsltproc $^ | xmllint --schema tools/gpx.xsd --encode utf-8 --format --nsclean --output $@ -

all: map/index.gpx
	rsync -aqP static/ map/

map/index.ttl: $(wildcard *.ttl)
	mkdir -p map
	cat $^ > $@

clean:
	rm -rf map
