<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  xmlns="http://www.topografix.com/GPX/1/1"
  xmlns:g="http://www.topografix.com/GPX/1/1#"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="gpx rdf"
  version="1.0">
  <xsl:output method="xml"/>

  <xsl:template match="/">
    <gpx version="1.1" creator="https://Marcus.Rohrmoser.name">
      <metadata>
        <name>Shared Hosting Vendors running #Seppos</name>
        <author>
          <name>Mro</name>
          <link href="https://Marcus.Rohrmoser.name"/>
        </author>
        <time>2024-02-15T09:30:00+01:00</time>
        <keywords>Shared Hosting Vendor</keywords>
      </metadata>
      <xsl:apply-templates select="*"/>
    </gpx>
  </xsl:template>

  <xsl:template match="rdf:Description">
    <xsl:variable name="latlon" select="substring-before(substring-after(@rdf:about,'geo:'),'?')"/>
    <wpt lat="{substring-before($latlon,',')}" lon="{substring-after($latlon,',')}">
      <name><xsl:value-of select="g:name"/></name>
      <desc>&lt;a href="<xsl:value-of select="g:url"/>"&gt;<xsl:value-of select="g:url"/>&lt;/a&gt;</desc>
      <sym>subject#hoster</sym>
	</wpt>
  </xsl:template>
</xsl:stylesheet>
